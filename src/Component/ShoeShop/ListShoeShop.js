import React, { Component } from 'react';
import ItemShoesShop from './ItemShoesShop';

export default class ListShoeShop extends Component {
	renderListShoeShop() {
		return this.props.shoesArr.map((item) => {
			return (
				<ItemShoesShop
					handleViewDetail={this.props.handleChangeDetail}
					handleAddToCart={this.props.handleAddToCart}
					data={item}
				/>
			);
		});
	}
	render() {
		return <div className="row">{this.renderListShoeShop()}</div>;
	}
}
