import React, { Component } from 'react';
import Cart from './Cart';
import { dataShoesShop } from './data';
import DetailShoeShop from './DetailShoeShop';
import Header from './Header';
import ListShoeShop from './ListShoeShop';

export default class ShoeShop extends Component {
	state = {
		shoesArr: dataShoesShop,
		detail: [],
		cart: [],
	};

	handleChangeDetail = (value) => {
		this.setState({ detail: value });
	};

	handleAddToCart = (shoe) => {
		let cloneCart = [...this.state.cart];
		let index = this.state.cart.findIndex((item) => {
			return item.id == shoe.id;
		});

		if (index == -1) {
			let cartItem = { ...shoe, number: 1 };
			cloneCart.push(cartItem);
		} else {
			cloneCart[index].number++;
		}

		this.setState({
			cart: cloneCart,
		});
	};

	render() {
		return (
			<>
				<Header cart={this.state.cart} />
				<div className="center container">
					<Cart cart={this.state.cart} />
					<DetailShoeShop detail={this.state.detail} />
					<ListShoeShop
						handleChangeDetail={this.handleChangeDetail}
						handleAddToCart={this.handleAddToCart}
						shoesArr={this.state.shoesArr}
					/>
				</div>
			</>
		);
	}
}
