import React, { Component } from 'react';

export default class Cart extends Component {
	renderTbody = () => {
		return this.props.cart.map((item) => {
			return (
				<tr>
					<td>{item.id}</td>
					<td>
						<img style={{ width: '80px' }} src={item.image} alt="" />
					</td>
					<td>{item.name}</td>
					<td>{item.price * item.number}</td>

					<td>
						<div className="units">
							<div
								onClick={() => {
									this.props.handleGiamSoLuong(1);
								}}
								className="btn minus">
								-
							</div>
							<div className="number">{item.number}</div>
							<div onClick={this.props.handleTangSoLuong} className="btn plus">
								+
							</div>
						</div>
					</td>
				</tr>
			);
		});
	};
	render() {
		return (
			<table className="table">
				<thead>
					<tr>
						<th>ID</th>
						<th>Hình ảnh</th>
						<th>Tên sản phẩm</th>
						<th>Giá</th>
						<th>Số lượng</th>
					</tr>
				</thead>
				<tbody>{this.renderTbody()}</tbody>
			</table>
		);
	}
}
