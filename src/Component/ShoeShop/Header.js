import React, { Component } from "react";

export default class Header extends Component {
  render() {
    return (
      <header className="header">
        <div className="search">
          <div className="form">
            <i className="fa-solid fa-magnifying-glass" />
            <input type="text" placeholder="Tìm kiếm sản phẩm,..." />
            <button>Tìm kiếm</button>
          </div>
        </div>
        <button
          onClick={() => {
            document.getElementById("myModal").style.display = "block";
          }}
          className="btn__cart"
        >
          <i className="fa-solid fa-cart-shopping" />
          <div className="count__cart">
            <div id="count__cart">{this.props.cart.length}</div>
          </div>
        </button>
        <btn className="manager__product btn">
          <i className="fa fa-user-alt" />
          <span>
            <a href="../Phan2-Quantri/index.html">Quản lý sản phẩm</a>
          </span>
        </btn>
      </header>
    );
  }
}
