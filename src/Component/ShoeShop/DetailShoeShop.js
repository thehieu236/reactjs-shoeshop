import React, { Component } from 'react';

export default class DetailShoeShop extends Component {
	render() {
		return (
			<div className="view__dentail p-2">
				<h3>Chi tiết sản phẩm: </h3>
				<div className="row">
					<img src={this.props.detail.image} className="col-4" alt="" />
					<div className="col-8 mt-3">
						<p>Tên: {this.props.detail.name}</p>
						<p>Giá: {this.props.detail.price} $</p>
						<p>Mô tả: {this.props.detail.description}</p>
					</div>
				</div>
			</div>
		);
	}
}
