import React, { Component } from "react";

class ItemShoesShop extends Component {
  render() {
    let { id, image, name, description, price, quantity } = this.props.data;
    return (
      <div key={id} className="item col-3">
        <div className="card">
          <div className="img">
            <img src={image} alt="" />
          </div>
          <div className="content">
            <div className="title">{name}</div>
            <div className="desc">{description}</div>
            <div className="price">
              Giá:
              <p>{price}$</p>
            </div>
            <button
              onClick={() => {
                this.props.handleAddToCart(this.props.data);
              }}
              className="add"
            >
              Thêm vào giỏ hàng
            </button>
            <button
              onClick={() => {
                this.props.handleViewDetail(this.props.data);
              }}
              className="detail"
            >
              Chi tiết
            </button>
          </div>
        </div>
      </div>
    );
  }
}

export default ItemShoesShop;
