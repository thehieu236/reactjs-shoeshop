import './App.css';
import ShoeShop from './Component/ShoeShop/ShoeShop';

function App() {
	return (
		<div className="App">
			<ShoeShop />
		</div>
	);
}

export default App;
